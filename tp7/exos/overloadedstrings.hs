-- {-# LANGUAGE OverloadedStrings #-}

import Data.Text as T

newtype Person = Person Text deriving Show

persons :: [Person]
persons = [Person (T.pack "John"), Person (T.pack "Haskell")]

main :: IO ()
main = print persons

