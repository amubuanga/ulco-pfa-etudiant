import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

readInt :: Int -> IO ()
readInt i = do
    putStr $ "saisie " ++ show i ++ " : "
    hFlush stdout
    res <- getLine
    let mx = readMaybe res :: Maybe Int
    case mx of 
        Just x  -> putStrLn $ "Vous avez saisi l'entier " ++ show x
        Nothing -> putStrLn "saisie invalide"

main :: IO ()
main = forM_ [1..4] readInt


