
fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 [] = []
fizzbuzz1 (x:xs) = str : fizzbuzz1 xs
    where str3 = if x `mod` 3 == 0 then "fizz" else ""
          str5 = if x `mod` 5 == 0 then "fizz" else ""
          str35 = str3 ++ str5
          str = if str35 /= "" then str35 else show x

fizzbuzz2 :: [Int] -> [String]
fizzbuzz2 [] = []
fizzbuzz2 (x:xs)  
    | x `mod` 15 == 0 = "fizzbuzz" : fizzbuzz2 xs
    | x `mod` 5 == 0 = "buzz" : fizzbuzz2 xs
    | x `mod` 3 == 0 = "fizz" : fizzbuzz2 xs
    | otherwise = show x : fizzbuzz2 xs


-- fizzbuzz

main :: IO ()
main = do
   print $ fizzbuzz1 [1..15]
   print $ fizzbuzz2 [1..15]

