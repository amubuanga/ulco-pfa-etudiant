import Data.List

-- threshList
threshList :: Ord a => a -> [a] -> [a]
threshList s xs = map (min s) xs

-- selectList
selectList :: Ord  a => a -> [a] -> [a]
selectList s = filter (<s)

--maxList
maxList :: (Foldable t, Ord a) => t a -> a
maxList = foldr1 max 

main :: IO ()
main = do
    print $ threshList 3 [1..5::Int]
    print $ selectList 3 [13,42,37 :: Int]
    print $ maxList [13, 42, 37::Int]


