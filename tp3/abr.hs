import Data.List (foldl')

data Abr a = Empty | Node a (Abr a) (Abr a)
    deriving (Show)

insererAbr :: Ord t => t -> Abr t -> Abr t
insererAbr x Empty = Node x Empty Empty
insererAbr x (Node y l r) =
    if x < y 
    then Node y (insererAbr x l) r 
    else Node y l (insererAbr x r)

listToAbr :: Ord a => [a] -> Abr a
listToAbr [] = Empty
listToAbr (x:xs) = insererAbr x (listToAbr xs)

abrToList :: Abr a -> [a]
abrToList Empty = []
abrToList (Node x l r) = abrToList l ++ [x] ++ abrToList r

main :: IO ()
main = do
    print $ listToAbr [12,42]
    print $ abrToList $ listToAbr [42,13,37]

