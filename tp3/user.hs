
-- TODO User
data User = User 
    {_nom :: String
    ,_prenom :: String
    ,_age :: Int
    } deriving Show

showUser :: User -> String
showUser (User nom prenom age) = nom ++ " " ++ prenom ++ " " ++ show age ++ " ans"

incAge :: User -> User
incAge u = u {_age = _age u + 1}

main :: IO ()
main = do
    let u1 = User "foo" "bar" 42
    print u1
    putStrLn $ showUser u1

