
data List a
    = Nil
    | Cons a (List a) deriving Show

sumList :: Num a => List a -> a
sumList Nil = 0
sumList (Cons x xs) = x + sumList xs

concatList  :: List a -> List a -> List a 
concatList Nil ys = ys
concatList (Cons x xs) ys = Cons x (concatList xs ys)
-- flatList 

toHaskell :: List a -> [a]
toHaskell Nil = []
toHaskell (Cons x xs) = x:toHaskell xs

fromHaskell :: [a] -> List a
fromHaskell [] = Nil
fromHaskell (x:xs) = Cons x (fromHaskell xs)

myShowList :: Show a => List a -> String
myShowList Nil = ""
myShowList (Cons x xs) = show x ++ " " ++ myShowList xs

main :: IO ()
main = do
    print $ (sumList ((Cons 1 (Cons 2 Nil))))
    print $ concatList ((Cons 1 (Cons 2 Nil))) ((Cons 3 (Cons 4 Nil)))
    print $ myShowList ((Cons "foo" (Cons "bar" Nil)))
    print $ toHaskell ((Cons 1 (Cons 2 Nil)))
    print $ fromHaskell [1,2,3,4]
